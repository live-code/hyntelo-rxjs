import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { ConfigService } from './core/config.service';

@Component({
  selector: 'hy-root',
  template: `
    <div [ngClass]="{
        'darkMode': (themeService.theme$ | async) === 'dark',
        'lightMode': (themeService.theme$ | async) === 'light'
    }">
      <button routerLink="login">login</button>
      <button routerLink="admin">admin</button>
      <button routerLink="demo1">Demo1</button>
      <button routerLink="demo2/5">Demo2</button>
      <button routerLink="demo3">Demo3</button>
      <button
        (click)="authService.logout()"
        *ngIf="authService.isLogged$ | async">logout</button>
      {{themeService.config$ | async | json}}
      {{(authService.displayName$ | async)}}
    </div>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .darkMode { background-color: #888; padding: 20px}
    .lightMode { background-color: cyan; padding: 20px}
  `]
})
export class AppComponent {
  constructor(
    public themeService: ConfigService,
    public authService: AuthService
  ) {
  }
}
