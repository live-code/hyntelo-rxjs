import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { catchError, iif, mergeMap, Observable, of, withLatestFrom } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
    authService.token$
      .subscribe(console.log)
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => iif(
          () => isLogged,
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + token } })),
          next.handle(request),
        )),
        catchError(err => {
          return of(err);
        })
      )
  }

  intercept2(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        withLatestFrom(this.authService.token$),
        mergeMap(([isLogged, token]) => {
          if (isLogged) {
            return next.handle(request.clone({
              setHeaders: {
                Authorization: 'Bearer ' + token
              }
            }))
          }
          return next.handle(request)
        })
      )

  }
}
