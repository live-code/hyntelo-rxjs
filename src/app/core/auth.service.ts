import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';
import { Auth } from '../model/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  auth$ = new BehaviorSubject<Auth | null>(null);

  constructor() {
    const auth: string | null = localStorage.getItem('auth');

    if (auth) {
      this.auth$.next(JSON.parse(auth) as Auth);
    }
  }

  login(data: { username?: string; password?: string }): void {
    console.log('credential', data);
    // simulate login request
    setTimeout(() => {
      const fakeResponse: Auth = {
        token: 'abc123',
        displayName: 'Mario Rossi',
        role: 'admin', // using 'guest' you should not see the CMS button in navbar
      };

      this.auth$.next(fakeResponse)
      window.localStorage.setItem('auth', JSON.stringify(fakeResponse));

    }, 500);
  }

  logout() {
    this.auth$.next(null)
    window.localStorage.removeItem('auth');
  }

  get isLogged$() {
    return this.auth$
      .pipe(
        map(auth => !!auth)
      )
  }

  get role$() {
    return this.auth$
      .pipe(
        map(auth => auth?.role)
      )
  }

  get token$() {
    return this.auth$
      .pipe(
        map(auth => auth?.token)
      )
  }

  get displayName$() {
    return this.auth$
      .pipe(
        map(auth => auth?.displayName)
      )
  }
}
