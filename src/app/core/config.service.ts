import { Injectable } from '@angular/core';
import { BehaviorSubject, map } from 'rxjs';

export interface Config {
  theme: Theme;
  lang: 'it' | 'en'
}
export type Theme = 'dark' | 'light';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  config$ = new BehaviorSubject<Config>({ theme: 'dark', lang: 'it'});

  setTheme(val: Theme) {
    this.config$.next({
      ...this.config$.getValue(),
      theme: val
    })
  }

  get theme$() {
    return this.config$
      .pipe(
        map(config => config.theme)
      )
  }



}
