import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  BehaviorSubject,
  buffer, combineLatest,
  concatMap, debounceTime,
  delay,
  filter,
  interval,
  map,
  mergeMap,
  Subject,
  switchMap,
  tap,
  toArray, withLatestFrom
} from 'rxjs';
import { User } from '../../model/user';

@Component({
  selector: 'hy-demo3',
  template: `
    <input type="text" [formControl]="input">

    <li *ngFor="let u of data">
      {{u.user.name}}
      <ul>
        <li *ngFor="let todo of u.todos">
          <input type="checkbox" [ngModel]="todo.completed">
          {{todo.title}}
        </li>
      </ul>
    </li>
    
  `,
})
export class Demo3Component  {
  data: { user: User, todos: any[] }[] = [];
  input = new FormControl('');
  completedOnly = new BehaviorSubject<boolean>(true)
  anything = new BehaviorSubject(123)

  constructor(private http: HttpClient) {

    this.input.valueChanges
      .pipe(
        debounceTime(1000),
        mergeMap(val => http.get<User[]>("https://jsonplaceholder.typicode.com/users?q=" + val)
          .pipe(
            switchMap(users => users),
            withLatestFrom(this.completedOnly),
            concatMap(params => this.getTodos(params[0], params[1])),
            toArray()
          )),
      )
      .subscribe(user => {
        this.data = user;
      })

  }

  getTodos(user: User, isCompleted: boolean) {
    return this.http.get<any[]>(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}&completed=${isCompleted}`)
      .pipe(
        delay(100),
        map(todos => ({  user, todos  }))
      )
  }
}
