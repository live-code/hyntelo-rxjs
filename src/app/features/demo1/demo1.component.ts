import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  catchError,
  debounceTime,
  delay,
  distinctUntilChanged,
  filter,
  map,
  mergeAll,
  mergeMap,
  Observable, of
} from 'rxjs';
import { Meteo } from '../../model/meteo';
import { User } from '../../model/user';

@Component({
  selector: 'hy-demo1',
  template: `
   
    
    <input type="text" [formControl]="input">
    <br>
    {{ displayName$ | async}}
    <ng-template #loader>
      loading....
    </ng-template>
    {{ 2 | displayName | async}}
    
    <h1>Meteo</h1>
    {{meteo$ | async}} °
  `,
})
export class Demo1Component {
  input = new FormControl('', { nonNullable: true })
  meteo$ = this.input.valueChanges
    .pipe(
      filter(text => text.length > 3),
      debounceTime(1000),
      distinctUntilChanged(),
      mergeMap(
        text => this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
          .pipe(
            catchError(err => of(null))
          )
      ),
      // filter(meteo => !!meteo),
      map(meteo => meteo?.main.temp)
      // map(meteo => meteo ? meteo?.main.temp : 'not available')
    )

  displayName$ = this.http.get<User>('https://jsonplaceholder.typicode.com/users/1')
    .pipe(
      delay(1000),
      map(res => `${res.name} (${res.phone})`)
    )

  constructor(private http: HttpClient) {


  }
}

