import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, filter, forkJoin, iif, map, mergeMap, of, subscribeOn, tap } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';

@Component({
  selector: 'hy-demo2',
  template: `
    <p>
      POST ID: {{currentId}}
    </p>
    
    <button [routerLink]="'/demo2/11'">11</button>
    <button [routerLink]="'/demo2/22'">22</button>
    <button [routerLink]="'/demo2/33'">33</button>
    
  <pre>{{data | json}}</pre>
  `,
})
export class Demo2Component {
  data: Data | null = null;
  currentId: number = 0;

  constructor(private http: HttpClient, activatedRouter: ActivatedRoute, private router: Router) {
    activatedRouter.params
      .pipe(
        mergeMap(params => {
          return  iif(
            () => !!params['id'],
            this.getUserAndPostInfo(params['id']),
            this.http.get<Post>('https://jsonplaceholder.typicode.com/users')
          )
        })
      )
      .subscribe(console.log)
  }

  getUserAndPostInfo(id: number) {
    return this.http.get<Post>('https://jsonplaceholder.typicode.com/posts/' + id)
      .pipe(
        mergeMap(
          post => this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + post.userId )
            .pipe(
              map(user => ({ post, user }) ),
            )
        )
      );
  }

}

type Data = { user: User, post: Post}
