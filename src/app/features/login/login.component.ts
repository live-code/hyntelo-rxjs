import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'hy-login',
  template: `
    <h1>Login Simulation</h1>
    <form [formGroup]="form" (submit)="authService.login(form.value)">
      <input formControlName="username" />
      <input formControlName="password" />
      <button type="submit">SIGN IN</button>
    </form>

  `,
  styles: [
  ]
})
export class LoginComponent  {
  form = this.fb.nonNullable.group({
    username: ['anyUser', Validators.required],
    password: ['anyPass', Validators.required],
  });
  constructor(private fb: FormBuilder, public authService: AuthService) {}

}
