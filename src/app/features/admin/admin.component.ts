import { HttpClient } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { AsyncSubject, BehaviorSubject, interval, ReplaySubject, share, Subject } from 'rxjs';
import { ConfigService } from '../../core/config.service';
import { User } from '../../model/user';

@Component({
  selector: 'hy-admin',
  template: `
    <li *ngFor="let user of (users$ | async)">
      {{user.name}}
    </li>
    <button (click)="themeService.setTheme('light')">Light</button>
    <button (click)="themeService.setTheme('dark')">Dark</button>
  `,
})
export class AdminComponent implements OnInit {
  users$ = this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      share()
    )

  subject = new ReplaySubject(1)

  constructor(
    public themeService: ConfigService,
    private http: HttpClient,
    ) {
  }

  ngOnInit(): void {
  }

}

