import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { delay, map, Observable } from 'rxjs';
import { User } from '../model/user';

@Pipe({
  name: 'displayName'
})
export class UserPipe implements PipeTransform {
  constructor(private http: HttpClient) {}

  transform(id: number): Observable<string> {
    return this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
      .pipe(
        delay(1000),
        map(res => `${res.name} (${res.phone})`)
      );
  }

}
